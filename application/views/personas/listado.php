<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Listado de personas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

</head>
<body>
    <h1 class="text-center">Listado de personas</h1>
    <div class="container">
    <br>
        <a href="guardar" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Agregar Persona Form</a>
 
        <button class="btn btn-success" onclick="agregar_persona()"><i class="glyphicon glyphicon-plus"></i>Agregar Persona Ajax</button>
    <br>
    <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">Edad</th>
            <th scope="col">Email</th>
            <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($personas as $key => $p) : ?>
                <tr>
                <th scope="row"><?php echo $p->persona_id; ?></th>
                <td><?php echo $p->nombre; ?></td>
                <td><?php echo $p->apellido; ?></td>
                <td><?php echo $p->edad; ?></td>
                <td><?php echo $p->email; ?></td>
                <td>
                    <button class="btn btn-primary" onclick="ver_persona(<?php echo $p->persona_id;?>)">Ver</button>
                    <button class="btn btn-warning" onclick="editar_persona(<?php echo $p->persona_id;?>)">Editar</button>
                    <!-- <button class="btn btn-success" onclick="unir_persona_campania(<?php //echo $p->persona_id;?>)" >Agregar Campaña</button> -->
                   <!--  <a class="btn btn-warning" href="guardar/<?php //echo $p->persona_id; ?>">Editar</a>
                    <br> -->
                    <a href="#"  class="btn btn-danger"
                        data-toggle="modal" 
                        data-target="#dlgDelete" 
                        data-id="<?php echo $p->persona_id; ?>"
                        data-name="<?php echo $p->nombre; ?>">Borrar</a>
                </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        </table>

        <div class="modal fade" id="dlgDelete" tabindex="-1" role="dialog" aria-labelledby="dlgDeleteLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dlgDeleteLabel">
                Se eliminará la persona: 
                <span></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- <div class="modal-body">
            </div> -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" id="b-borrar">Borrar</button>
            </div>
            </div>
        </div>
        </div> <!-- Fin Dialog ELiminar -->
        
        <!-- Dialog Guardar -->
        <div class="modal fade" id="dlgGuardar" tabindex="-1" role="dialog" aria-labelledby="dlgGuardarLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dlgGuardarLabel">Agregar Persona</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span></span>
                <form action="#" id="frmGuardar" class="form-horizontal">
                    <input type="hidden" value="" name="persona_id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" id="nombre" placeholder="Nombre..." class="form-control" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido</label>
                            <div class="col-md-9">
                                <input name="apellido" id="apellido" placeholder="Apellido..." class="form-control" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input name="email" id="email" placeholder="Email..." class="form-control" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Edad</label>
                            <div class="col-md-9">
                                <input name="edad" placeholder="Edad" id="edad" class="form-control" type="number">
                            </div>
                        </div>
                    </div>
                </form>
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="b-guardar" onclick="guardar_persona()">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
            </div>
        </div>
        </div> <!-- Fin Dialog Agregar / Editar -->

        <!-- Dialog Ver -->
        <div class="modal fade" id="dlgVer" tabindex="-1" role="dialog" aria-labelledby="dlgVerLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dlgVerLabel">Ver Persona</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="frmVer" class="form-horizontal">
                    <input type="hidden" value="" name="persona_id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="persona_id" placeholder="id" class="form-control" type="text" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" placeholder="Nombre..." class="form-control" type="text" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido</label>
                            <div class="col-md-9">
                                <input name="apellido" placeholder="Apellido..." class="form-control" type="text" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Edad</label>
                            <div class="col-md-9">
                                <input name="edad" placeholder="Edad" class="form-control" type="number" disabled>
                            </div>
                        </div>
                    </div>
                </form>
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
            </div>
            </div>
        </div>
        </div> <!-- Fin Dialog ver -->
        

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
        <script>
            var id;
            var link;
            var save_method; //for save method string
            var table;

            function ver_persona(id)
            {
                $('#frmVer')[0].reset();

                $.ajax({
                    url : "<?php echo site_url('personas/buscar_ajax/');?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    {

                        $('[name="persona_id"]').val(data.persona_id);
                        $('[name="nombre"]').val(data.nombre);
                        $('[name="apellido"]').val(data.apellido);
                        $('[name="edad"]').val(data.edad);
                        $('[name="email"]').val(data.email);

                        $('#dlgVer').modal('show');
                        $('.modal-title').text('Ver Persona');

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
            }

            function unir_persona_campania(id)
            {
                alert('Id: '+id);
                $.ajax({
                    url : "<?php echo site_url('personas/unir_campania_ajax/');?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    {

                      
                        alert("Agregado a campaña");

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
            }

            function agregar_persona()
            {
                save_method = 'add';
                $('#frmGuardar')[0].reset();
                $('#dlgGuardar').modal('show');
            }

            function editar_persona(id)
            {
                save_method = 'update';
                $('#frmGuardar')[0].reset();

                $.ajax({
                    url : "<?php echo site_url('personas/buscar_ajax/');?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    {

                        $('[name="persona_id"]').val(data.persona_id);
                        $('[name="nombre"]').val(data.nombre);
                        $('[name="apellido"]').val(data.apellido);
                        $('[name="edad"]').val(data.edad);
                        $('[name="email"]').val(data.email);

                        $('#dlgGuardar').modal('show');
                        $('.modal-title').text('Editar Persona');

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
            }

            function guardar_persona()
            {
                var url;
                if(save_method == 'add')
                {
                    url = "<?php echo site_url('personas/guardar_ajax')?>";
                }
                else
                {
                    url = "<?php echo site_url('personas/actualizar_ajax')?>";
                }


                $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#frmGuardar').serialize(),
                    dataType: "JSON",
                    success: function(data)
                    {
                        if(data.status)
                        {
                            $('#dlgGuardar').modal('hide');
                            location.reload();         
                        }
                        else
                        {
                            var modal = $("#dlgGuardar")
                            modal.find('.modal-body span').html(data.errores).find('p').addClass('alert alert-warning')
                        }
                        
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error adding / update data');
                    }
                });
            }

            $('#dlgDelete').on('show.bs.modal', function (event) {
                link = $(event.relatedTarget)
                id = link.data('id') 
                var nombre = link.data('name') 
                var modal = $(this)
                modal.find('.modal-title span').text(nombre)
                })


            $('#b-borrar').click(function(){
                $.ajax({
                    url:"<?php echo base_url();?>personas/borrar_ajax/"+id,
                    context:document.body
                }).done(function(res){
                    $(link).parent().parent().remove();
                    $("#dlgDelete").modal('hide');
                });
            });


        </script>

    </div> <!-- Fin div.container -->

</body>
</html>