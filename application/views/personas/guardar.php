<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Personas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    
    <h1 class="text-center">Listado de personas</h1>

    <div class="container">
        <br>
            <a href="<?php echo base_url(); ?>personas/listado" class="btn btn-success">Regresar</a>
        <br>
        <br>
        <?php 
            if (null !== validation_errors() && strlen(validation_errors()) > 0) 
                {echo "<div class='alert alert-warning'>";echo validation_errors();echo "</div>";}
            if (isset($message_display)) 
                {echo "<div class='alert alert-warning'>"; echo $message_display; echo "</div>"; }?>
        <?php echo form_open(''); ?>

            <div class="form-group">
                <?php
                    echo form_label('Nombre','nombre');
                    $input = array(
                        'name'  => 'nombre',
                        'value' => $nombre,
                        'class' => 'form-control input-lg'
                    );
                    echo form_input($input);

                    echo form_label('Apellido','apellido');
                    $input = array(
                        'name'  => 'apellido',
                        'value' => $apellido,
                        'class' => 'form-control input-lg'
                    );
                    echo form_input($input);

                    echo form_label('Edad','edad');
                    $input = array(
                        'name'  => 'edad',
                        'value' => $edad,
                        'type' => 'number',
                        'class' => 'form-control input-lg'
                    );
                    echo form_input($input);
                ?>
            </div>

        <?php
            echo form_submit('mysubmit', 'Enviar',"class='btn btn-primary'"); 
            echo form_close(); 
        ?>
    </div>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
</body>
</html>