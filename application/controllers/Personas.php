<?php 
class Personas extends CI_Controller{

    public function __construct()
	{
		parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');

        $this->load->model('Persona');
        $this->load->library('form_validation');
        $this->load->library('MailChimp');

	}

    public function index()
    {
        redirect("personas/listado");
    }

    public function mail_chimp_list()
    {

        $MailChimp = new MailChimp('73c1adf4182a09181ebd4de3c59aface-us20');
        $list_id = '12ada72e10';

        $result = $MailChimp->get('lists');

        print_r($result);
    }

    public function mail_campaigns()
    {

        $MailChimp = new MailChimp('73c1adf4182a09181ebd4de3c59aface-us20');
        $list_id = '12ada72e10';

        $result = $MailChimp->get('campaigns');

        print_r($result);
    }

    public function listado()
    {

        $dataPersona["personas"]=$this->Persona->findAll();
        
        $this->load->view('personas/listado',$dataPersona);
    }

    public function unir_campania_ajax($persona_id = null)
    {

        $persona = $this->Persona->findById($persona_id);

        //$email = 'abc@gmail.com';
        $list_id = '12ada72e10';
        $api_key = '73c1adf4182a09181ebd4de3c59aface-us20';
        
        $data_center = substr($api_key,strpos($api_key,'-')+1);
        
        $url = 'https://'. $data_center .'.api.mailchimp.com/3.0/lists/'. $list_id .'/members';
        
        $json = json_encode([
            'email_address' => $persona->email,
            'merge_fields' => ['FNAME'=>$persona->nombre, 'LNAME'=>$persona->apellido],
            'status'        => 'subscribed', //pass 'subscribed' or 'pending'
        ]);
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $api_key);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        echo $status_code;

    }

    public function guardar_ajax()
    {
        $MailChimp = new MailChimp('73c1adf4182a09181ebd4de3c59aface-us20');
        $list_id = '12ada72e10';

        $this->form_validation->set_rules('nombre', 'Nombre', 'required',array('required' => 'El valor %s es requerido.'));
        $this->form_validation->set_rules('apellido', 'Apellido', 'required',array('required' => 'El valor %s es requerido.'));
        $this->form_validation->set_rules('edad', 'Edad', 'required',array('required' => 'El valor %s es requerido.'));

        if($this->input->server("REQUEST_METHOD") == "POST" ){
            $data["nombre"]=$this->input->post('nombre');
            $data["apellido"]=$this->input->post('apellido');
            $data["email"]=$this->input->post('email');
            $data["edad"]=$this->input->post('edad');

            if ($this->form_validation->run()){
                $this->Persona->insert($data);

                //$list_id = 'b1234346';

                $result = $MailChimp->post("lists/$list_id/members", [
                                'email_address' => $data["email"],
                                'merge_fields' => ['FNAME'=>$data["nombre"], 'LNAME'=>$data["apellido"]],
                                'status'        => 'subscribed',
                            ]);

                //print_r($result);


                echo json_encode(array("status" => TRUE));
            }
            else{
                echo json_encode(array("status" => FALSE, "errores" => validation_errors()));
            }
        }
        
    }

    public function actualizar_ajax()
	{
        $MailChimp = new MailChimp('73c1adf4182a09181ebd4de3c59aface-us20');
        $list_id = '12ada72e10';

		$data = array(
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
                'edad' => $this->input->post('edad'),
                'email' => $this->input->post('email')
            );
            
        $subscriber_hash = $MailChimp->subscriberHash($data["email"]);

        $result = $MailChimp->patch("lists/$list_id/members/$subscriber_hash", [
                        'merge_fields' => ['FNAME'=>$data["nombre"], 'LNAME'=>$data["apellido"]],
                    ]);
        //'interests'    => ['2s3a384h' => true],

        $persona_id = $this->input->post('persona_id');
        $this->Persona->update($persona_id,$data);


		echo json_encode(array("status" => TRUE));
    }
    
    public function buscar_ajax($persona_id)
    {
        $data = $this->Persona->findById($persona_id);

        echo json_encode($data);
    }

    public function guardar($persona_id = null)
    {
        
        $this->form_validation->set_rules('nombre', 'Nombre', 'required', array('required' => 'El valor %s es requerido.')
        );
        $this->form_validation->set_rules('apellido', 'Apellido', 'required',array('required' => 'El valor %s es requerido.'));
        $this->form_validation->set_rules('edad', 'Edad', 'required',array('required' => 'El valor %s es requerido.'));

        $dataPersona["nombre"]="";$dataPersona["apellido"]="";$dataPersona["edad"]="";
        if(isset($persona_id)){
            $persona = $this->Persona->findById($persona_id);

            if(isset($persona)){
                $dataPersona["nombre"]=$persona->nombre;
                $dataPersona["apellido"]=$persona->apellido;
                $dataPersona["edad"]=$persona->edad;
                $dataPersona["email"]=$persona->email;
            }
        }

        if($this->input->server("REQUEST_METHOD") == "POST" ){
            $data["nombre"]=$this->input->post('nombre');
            $data["apellido"]=$this->input->post('apellido');
            $data["edad"]=$this->input->post('edad');
            $data["email"]=$this->input->post('email');

            $dataPersona["nombre"]=$this->input->post('nombre');
            $dataPersona["apellido"]=$this->input->post('apellido');
            $dataPersona["edad"]=$this->input->post('edad');
            $dataPersona["email"]=$this->input->post('email');

            if ($this->form_validation->run()){
                if(isset($persona_id))
                    $this->Persona->update($persona_id,$data);
                else
                    $this->Persona->insert($data);
                    
                redirect("personas/listado");
                
            }

            
        }
        
        $this->load->view('personas/guardar',$dataPersona);
    }

    public function borrar($persona_id = null)
    {
        $this->Persona->delete($persona_id);

        redirect("personas/listado");
    }

    public function borrar_ajax($persona_id = null)
    {
        $MailChimp = new MailChimp('73c1adf4182a09181ebd4de3c59aface-us20');
        $list_id = '12ada72e10';
        $persona = $this->Persona->findById($persona_id);
        $subscriber_hash = $MailChimp->subscriberHash($persona->email);

        $MailChimp->delete("lists/$list_id/members/$subscriber_hash");
        $this->Persona->delete($persona_id);

        echo 1;
    }
}