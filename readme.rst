###################
Proyecto CodeIgniter
###################

Proyeco de prueba

*******************
Base de datos
*******************

CREATE DATABASE codeigniter;

**************************
Tabla
**************************

CREATE TABLE personas(
	persona_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    apellido VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    edad INTEGER NOT NULL
)

*******************
Visualizar
*******************

Link para visualizarlo `http://localhost/CodeIgniterAntonio/personas/listado`
